﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Google.OrTools.ConstraintSolver;

namespace Puzzles.Solutions
{
    class Binoxxo
    {
        public static void Start()
        {
            Solve(Input1.Binoxxo1);
        }

        /*
         * Create Model and Solve Sudoku:
         */
        private static int size;
        private static int boardsize;

        private static void Solve(string[,] input)
        {
            Double root = Math.Sqrt(input.Length);
            if ((root % 1) != 0 || (root % 2) != 0)
            {
                throw new ArgumentException("This is not a valid Binoxxo Puzzle.");
            }
            size = (int)(root);
            boardsize = size * size;


            var solver = new Solver("Binoxxo");

            
            // Board as 9x9 Matrix of Decision Variables in {1..boardsize}:
            IntVar[,] board = solver.MakeIntVarMatrix(size, size, 0, 1);

            // Pre-Conditions
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    switch(input[i, j])
                    {
                        case "X":
                            solver.Add(board[i, j] == 1);
                        break;
                        case "O":
                            solver.Add(board[i, j] == 0);
                        break;
                    }
                }
            }

            IEnumerable<int> range = Enumerable.Range(0, size);
            IEnumerable<int> range_2 = Enumerable.Range(0, size-2);
            IEnumerable<int> range_3 = Enumerable.Range(0, 3);


            List<IntVar> sums = new List<IntVar>();
            foreach (int i in range)
            {
                // Each Row / Column contains same amount of O and X (0/1 respectively): 
                solver.Add((from j in range select board[i, j]).ToArray().Sum().Var() == (size/2));
                solver.Add((from j in range select board[j, i]).ToArray().Sum().Var() == (size/2));

                // All 3 neigbours in every row/coloumn
                IntVar[][] threePacksHori = (from j in range_2 select (from k in range_3 select board[i,j+k]).ToArray()).ToArray();
                foreach(IntVar[] pack in threePacksHori)
                {
                    solver.Add(pack.Sum().Var() != 0);
                    solver.Add(pack.Sum().Var() != 3);
                }
                IntVar[][] threePacksVerti = (from j in range_2 select (from k in range_3 select board[j + k, i]).ToArray()).ToArray();
                foreach (IntVar[] pack in threePacksVerti)
                {
                    solver.Add(pack.Sum().Var() != 0);
                    solver.Add(pack.Sum().Var() != 3);
                }
            }
            
            // Start Solver:

            DecisionBuilder db = solver.MakePhase(board.Flatten(), Solver.INT_VAR_SIMPLE, Solver.INT_VALUE_SIMPLE);

            Console.WriteLine("Binoxxo:\n\n");

            solver.NewSearch(db);

            while (solver.NextSolution())
            {
                PrintSolution(board);
                Console.WriteLine();
            }

            Console.WriteLine("\nSolutions: {0}", solver.Solutions());
            Console.WriteLine("WallTime: {0}ms", solver.WallTime());
            Console.WriteLine("Failures: {0}", solver.Failures());
            Console.WriteLine("Branches: {0} ", solver.Branches());

            solver.EndSearch();

            Console.ReadKey();
        }


        private static void PrintSolution(IntVar[,] board)
        {
            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    Console.Write("[{0}] ", board[i, j].Value());
                }
                Console.WriteLine();
            }
        }

    }
}
