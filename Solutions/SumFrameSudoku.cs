﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Google.OrTools.ConstraintSolver;

namespace Puzzles.Solutions
{
    class SumFrameSudoku
    {
        public static void Start()
        {
            Solve(Input1.SumFrameSudoku1);
            
        }

        /*
         * Create Model and Solve Sudoku:
         */

        private static void Solve(Tuple<int[], int[], int[], int[]> input)
        {
            const int cellSize = 3;
            const int boardSize = cellSize * cellSize;

            if (
                input.Item1.Length != boardSize ||
                input.Item1.Length != boardSize ||
                input.Item1.Length != boardSize ||
                input.Item1.Length != boardSize
            )
            {
                throw new ArgumentException("This is not a valid 9x9 SumFrameSudoku Puzzle.");
            }
            
            var solver = new Solver("Sudoku");

            

            // Sudoku Board as 9x9 Matrix of Decision Variables in {1..9}:
            IntVar[,] board = solver.MakeIntVarMatrix(boardSize, boardSize, 1, boardSize);


            
            IEnumerable<int> zeroToTwo = Enumerable.Range(0, 3);
            IEnumerable<int> sixToEight = Enumerable.Range(6, 3);

            // Block 1-3 top
            for (int l = 0; l < boardSize; l++) {
                solver.Add((from h in zeroToTwo select board[l, h]).ToArray().Sum() == input.Item1[l]);
            }

           
            // Block 7-8 bottom
            for (int l = 0; l < boardSize; l++)
            {
                solver.Add((from h in sixToEight select board[l, h]).ToArray().Sum() == input.Item3[l]);
            }

            // Block 1 4 7 left
            for (int l = 0; l < boardSize; l++)
            {
                solver.Add((from h in zeroToTwo select board[h, l]).ToArray().Sum() == input.Item4[l]);
            }

            // Block 3 6 9 right
            for (int l = 0; l < boardSize; l++)
            {
                solver.Add((from h in sixToEight select board[h, l]).ToArray().Sum() == input.Item2[l]);
            }
            

            IEnumerable<int> range = Enumerable.Range(0, boardSize);

            // Each Row / Column contains only different values: 
            foreach (int i in range)
            {
                // Rows:
                solver.Add((from j in range select board[i, j]).ToArray().AllDifferent());

                // Columns:
                solver.Add((from j in range select board[j, i]).ToArray().AllDifferent());
            }

            IEnumerable<int> cell = Enumerable.Range(0, cellSize);

            // Each Sub-Matrix contains only different values:
            foreach (int i in cell)
            {
                foreach (int j in cell)
                {
                    solver.Add(
                        (from di in cell from dj in cell select board[i * cellSize + di, j * cellSize + dj]).ToArray()
                            .AllDifferent());
                }
            }

            // Start Solver:

            DecisionBuilder db = solver.MakePhase(board.Flatten(), Solver.INT_VAR_SIMPLE, Solver.INT_VALUE_SIMPLE);

            Console.WriteLine("SumFrameSudoku:\n\n");

            solver.NewSearch(db);

            while (solver.NextSolution())
            {
                PrintSolution(board);
                Console.WriteLine();
            }

            Console.WriteLine("\nSolutions: {0}", solver.Solutions());
            Console.WriteLine("WallTime: {0}ms", solver.WallTime());
            Console.WriteLine("Failures: {0}", solver.Failures());
            Console.WriteLine("Branches: {0} ", solver.Branches());

            solver.EndSearch();

            Console.ReadKey();
        }


        private static void PrintSolution(IntVar[,] board)
        {
            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    Console.Write("[{0}] ", board[i, j].Value());
                }
                Console.WriteLine();
            }
        }

    }
}
