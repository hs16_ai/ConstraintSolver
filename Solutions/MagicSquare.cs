﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Google.OrTools.ConstraintSolver;

namespace Puzzles.Solutions
{
    class MagicSquare
    {
        public static void Start()
        {
            Solve();
        }

        /*
         * Create Model and Solve Sudoku:
         */
        private const int size = 2;

        private static void Solve()
        {

            const int boardsize = size * size;

            var solver = new Solver("MagicSquare");

            
            // Board as 9x9 Matrix of Decision Variables in {1..boardsize}:
            IntVar[,] board = solver.MakeIntVarMatrix(size, size, 1, boardsize);

            solver.Add(board.Flatten().AllDifferent());
            
            IEnumerable<int> range = Enumerable.Range(0, size);

            // Each Row / Column contains only different values: 
            List<IntVar> sums = new List<IntVar>();
            foreach (int i in range)
            {
                // Rows:
                sums.Add((from j in range select board[i, j]).ToArray().Sum().Var());

                // Columns:
                sums.Add((from j in range select board[j, i]).ToArray().Sum().Var());

                
            }

            // Diagonal 1
            sums.Add((from j in range select board[j, j]).ToArray().Sum().Var());

            // Diagonal 2
            sums.Add((from j in range select board[j, size-1-j]).ToArray().Sum().Var());

            IntVar[] sumArray = sums.ToArray();
            for(int i = 1; i < sumArray.Length; i++)
            {
                solver.Add(sumArray[i - 1] == sumArray[i]);
            }

            
            // Start Solver:

            DecisionBuilder db = solver.MakePhase(board.Flatten(), Solver.INT_VAR_SIMPLE, Solver.INT_VALUE_SIMPLE);

            Console.WriteLine("MagicSqure:\n\n");

            solver.NewSearch(db);

            while (solver.NextSolution())
            {
                PrintSolution(board);
                Console.WriteLine();
            }

            Console.WriteLine("\nSolutions: {0}", solver.Solutions());
            Console.WriteLine("WallTime: {0}ms", solver.WallTime());
            Console.WriteLine("Failures: {0}", solver.Failures());
            Console.WriteLine("Branches: {0} ", solver.Branches());

            solver.EndSearch();

            Console.ReadKey();
        }


        private static void PrintSolution(IntVar[,] board)
        {
            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    Console.Write("[{0}] ", board[i, j].Value());
                }
                Console.WriteLine();
            }
        }

    }
}
