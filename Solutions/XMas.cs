﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Google.OrTools.ConstraintSolver;

namespace Puzzles.Solutions
{
    class XMas
    {
        public static void Start()
        {
            Solve(Input1.Xmas2);
            
        }

        /*
         * Create Model and Solve Xmas:
         */
        private static int size;
        private static void Solve(string[,] input)
        {
            Double root = Math.Sqrt(input.Length);
            if ((root % 1) != 0)
            {
                throw new ArgumentException("This is not a valid Xmas Puzzle.");
            }
            size = (int) (root);
            

            var solver = new Solver("Xmas");

            

            // Sudoku Board as 9x9 Matrix of Decision Variables in {1..9}:
            IntVar[,] board = solver.MakeIntVarMatrix(size, size, 0, 1);


            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++) {
                    if (!string.IsNullOrEmpty(input[i, j]) && !input[i, j].Equals(" "))
                    {
                        solver.Add(board[i, j] == 0); // unter zahlen sind keine geschenke
                        
                        int sum = Convert.ToInt32(input[i, j]);
                        solver.Add(getNeighbours(i,j,board).Sum() == sum);
                    }
                }
            }
            

            

            // Start Solver:

            DecisionBuilder db = solver.MakePhase(board.Flatten(), Solver.INT_VAR_SIMPLE, Solver.INT_VALUE_SIMPLE);

            Console.WriteLine("XMas:\n\n");

            solver.NewSearch(db);

            while (solver.NextSolution())
            {
                PrintSolution(board);
                Console.WriteLine();
            }

            Console.WriteLine("\nSolutions: {0}", solver.Solutions());
            Console.WriteLine("WallTime: {0}ms", solver.WallTime());
            Console.WriteLine("Failures: {0}", solver.Failures());
            Console.WriteLine("Branches: {0} ", solver.Branches());

            solver.EndSearch();

            Console.ReadKey();
        }

        private static IntVar[] getNeighbours(int x, int y, IntVar[,] board) {
            List<IntVar> l = new List<IntVar>();
            
            for (int i = -1; i < 2; i++) {
                if (x + i < 0 || x + i >= size) { continue; }
                for (int j = -1; j < 2; j++) {
                    if(y + j < 0 || y + j >= size || (i == 0 && j == 0)) { continue; }
                    l.Add(board[x + i, y + j]);
                }
            }            
            return l.ToArray(); ;
            
        }

        private static void PrintSolution(IntVar[,] board)
        {
            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    Console.Write("[{0}] ", board[i, j].Value());
                }
                Console.WriteLine();
            }
        }

    }
}
