﻿using System;
using System.Collections.Generic;
using Google.OrTools.ConstraintSolver;

/*
 * This model solves the knight tour puzzle presented in CP part 3.
 * 
 * Lecture: Introduction to Artificial Intelligence
 * Author: Marc Pouly
 * Version: 2014-8-6
 */

namespace Puzzles.Lecture
{
    public class KnightTour
    {
        /*
         * Main Method:
         */

        public static void Start()
        {
            Solve(6);
        }

        /*
         * Create model and solve problem:
         * Parameter determines chess board dimension
         */

        private static void Solve(int dim)
        {
            var solver = new Solver("Knight's Tour");

            /*
             * Chess board of size dim x dim: next[i] = where to jump from cell i
             * Start position = 0, end position = dim*dim (i.e. outside chessboard)
             */

            IntVar[] next = solver.MakeIntVarArray(dim*dim, 1, dim*dim);

            /*
             * Chess rules: 
             * For each cell (variable), eliminate all cells that cannot be reached from this position from the domain.
             */

            for (int i = 0; i < next.Length; i++)
            {
                next[i].SetValues(ReachableCells(i, dim));
            }

            /*
             * Variables that are part of the tour:
             */

            IntVar[] active = solver.MakeBoolVarArray(next.Length);

            foreach (IntVar t in active)
            {
                solver.Add(t == 1);
            }

            /*
             * Create a connected path:
             */

            solver.Add(next.AllDifferent());

            /*
             * Do not visit twice the same cell:
             */

            solver.Add(solver.MakeNoCycle(next, active));

            /*
             * Start solver:
             */

            DecisionBuilder db = solver.MakePhase(next, Solver.INT_VAR_SIMPLE, Solver.INT_VALUE_SIMPLE);

            solver.NewSearch(db);

            if (solver.NextSolution())
            {
                Console.WriteLine("The Knight's Tour starts in cell 0:\n\n");

                for (int i = 0; i < next.Length; i++)
                {
                    if (next[i].Value() != dim*dim)
                    {
                        Console.WriteLine("From cell " + i + " go to cell next[" + i + "] = " + next[i].Value());
                    }
                    else
                    {
                        Console.WriteLine("Cell " + i + " is the final cell of the knight's tour.");
                    }
                }

                Console.WriteLine("\n");
            }
            else
            {
                Console.WriteLine("No Knight's Tour found.\n");
            }

            Console.WriteLine("WallTime: {0}ms", solver.WallTime());
            Console.WriteLine("Failures: {0}", solver.Failures());
            Console.WriteLine("Branches: {0} ", solver.Branches());

            solver.EndSearch();

            Console.ReadKey();
        }

        /*
         * Return an array of cell numbers that can be reached from the current position by the knight figure.
         */

        private static long[] ReachableCells(int i, int dim)
        {
            var from = new List<long>();

            int cell = i - 2*dim - 1;

            if (cell >= 0 && cell/dim == i/dim - 2)
            {
                from.Add(cell);
            }

            cell = i - 2*dim + 1;

            if (cell >= 0 && cell/dim == i/dim - 2)
            {
                from.Add(cell);
            }

            cell = i - dim - 2;

            if (cell >= 0 && cell/dim == i/dim - 1)
            {
                from.Add(cell);
            }

            cell = i - dim + 2;

            if (cell >= 0 && cell/dim == i/dim - 1)
            {
                from.Add(cell);
            }

            cell = i + dim - 2;

            if (cell < dim*dim && cell/dim == i/dim + 1)
            {
                from.Add(cell);
            }

            cell = i + dim + 2;

            if (cell < dim*dim && cell/dim == i/dim + 1)
            {
                from.Add(cell);
            }

            cell = i + 2*dim - 1;

            if (cell < dim*dim && cell/dim == i/dim + 2)
            {
                from.Add(cell);
            }

            cell = i + 2*dim + 1;

            if (cell < dim*dim && cell/dim == i/dim + 2)
            {
                from.Add(cell);
            }

            // Any cell can be the last in a knight's tour:
            from.Add(dim*dim);

            return from.ToArray();
        }
    }
}