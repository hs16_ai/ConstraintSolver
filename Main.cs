
namespace Puzzles
{
    internal class MainClass
    {
        public static void Main(string[] args)
        {
            
            /**
             * Lecture:
             */

            //Lecture.Coloring.Start();
            //Lecture.Cryptogram.Start();
            //Lecture.EasterEgg.Start();
            //Lecture.Grocery.Start();
            //Lecture.KnightTour.Start();
            //Lecture.Sudoku.Start();
            //Lecture.Tsp.Start();
            //Lecture.Xkcd.Start();

            /**
             * Assignments:
             */
            
            //Solutions.SumFrameSudoku.Start();
            //Solutions.XMas.Start();
            //Solutions.MagicSquare.Start();
            Solutions.Binoxxo.Start();

            /**
             * Others: 
             */

            //Solutions.MagicSquare.Start();
            //Solutions.Xmas.Start();

        }
    }
}